import numpy as np
from numba import jit
from constants import EMBEDDING_LEN, char_token_map


def tokenize(seq: str):
    # return minified tokenized representations of text
    return np.array([char_token_map[c] for c in seq])


@jit(nopython=True)
def one_hot_encoding(x: int):
    return np.where(np.arange(EMBEDDING_LEN) == x, 1, 0)


if __name__ == "__main__":
    print(one_hot_encoding(1))
    print(tokenize("2+2=4"))
